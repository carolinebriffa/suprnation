# README #

### Prerequisites ###

* RabbitMQ needs to be running on localhost listening on default ports
* In root folder run mvn clean install

### Start the Producer ###

* Navigate to /producer
* Run mvn spring-boot:run

### Start the Consumer ###

* Navigate to /consumer
* Run mvn spring-boot:run