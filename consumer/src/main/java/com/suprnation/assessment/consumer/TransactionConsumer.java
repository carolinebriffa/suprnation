package com.suprnation.assessment.consumer;

import com.suprnation.assessment.data.Transaction;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class TransactionConsumer implements Consumer<Transaction> {

    private final ConcurrentHashMap<String, Integer> bets = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Integer> wins = new ConcurrentHashMap<>();
    private final Function<Integer, BiFunction<String, Integer, Integer>> summation = (i) -> (k, v) -> v == null ? i : v + i;

    @Override
    public void accept(Transaction transaction) {
        Integer betTotal = bets.compute(transaction.getSessionId(), summation.apply(transaction.getBet()));
        Integer winTotal = wins.compute(transaction.getSessionId(), summation.apply(transaction.getWin()));
        System.out.println(transaction.getUserId() + " " + transaction.getSessionId() + " " + betTotal + " " + winTotal);
    }
}