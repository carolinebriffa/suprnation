package com.suprnation.assessment.consumer;

import com.suprnation.assessment.data.Transaction;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
public class Application {

    private final ExecutorService executorService;
    private final Map<Integer, TransactionConsumer> transactionConsumerMap = new HashMap<>();

    @Bean
    SimpleMessageListenerContainer container(@Value("${transactions.queue.name}") String queueName,
                                             ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Application application) {
        return new MessageListenerAdapter(application, "handleMessage");
    }

    public Application(@Value("${consumer.threadPool.size}") Integer threadPoolSize) {
        executorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public void handleMessage(Message<Transaction> message) {
        Transaction transaction = message.getPayload();
        transactionConsumerMap.putIfAbsent(transaction.getUserId(), new TransactionConsumer());
        executorService.execute(() -> transactionConsumerMap.get(transaction.getUserId()).accept(transaction));
    }

}
