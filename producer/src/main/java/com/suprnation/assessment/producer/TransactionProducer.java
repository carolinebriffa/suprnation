package com.suprnation.assessment.producer;

import com.suprnation.assessment.data.Transaction;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class TransactionProducer {

    @Value("${transactions.queue.name}")
    private String queueName;

    private final String filePath = ClassLoader.getSystemResource("transactions.txt").getFile();

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendTransactions() throws IOException {
        Files.lines(Paths.get(filePath))
                .map(Transaction::fromFileInputString)
                .filter((t) -> t != null)
                .forEach(t -> rabbitTemplate.convertAndSend(queueName, MessageBuilder.withPayload(t).build()));
    }
}
