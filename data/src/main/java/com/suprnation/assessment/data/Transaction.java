package com.suprnation.assessment.data;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Transaction implements Serializable{

	int id;
	int userId;
	int bet;
	int win;
	String sessionId;

	public static Transaction fromFileInputString(String fileInputString){
		try {
			String[] values = fileInputString.split(" ");
			return new Transaction(
					Integer.valueOf(values[0]),
					Integer.valueOf(values[1]),
					Integer.valueOf(values[2]),
					Integer.valueOf(values[3]),
					values[4]);
		} catch (Exception e){
			return null;
		}
	}
}
